public class InchesToMillimeters {
public static void main(String[] args)
{
	int a =  2,	//inches
		b =  5,
		c = 10;
	
	double 	ax, //millimeters
			bx,
			cx;
	//1 inch = 25.4mm
	
	//convert inches to millimeters
	ax = a * 25.4;
	bx = b * 25.4;
	cx = c * 25.4;
	
	System.out.println(a + " inches is " + ax + " millimeters.");
	System.out.println(b + " inches is " + bx + " millimeters.");
	System.out.println(c + " inches is " + cx + " millimeters.");
}
}
