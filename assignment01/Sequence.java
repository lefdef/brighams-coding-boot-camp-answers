import java.awt.*;
    import java.applet.Applet;

    public class Sequence extends Applet
    {
    	/**
    	 * The main method in this class shows three circles 
		 * of different colors and sizes to the screen
    	 * 
    	 * @author  Brigham Diaz 
    	 * @version 09-03-08
    	 * @ta      11:50
    	 */
        public void paint (Graphics g)
        {
            
            
            g.setColor (Color.BLUE);
            g.fillOval (70, 70, 70, 70);

            g.setColor (Color.GREEN);
            g.fillRect (60, 60, 70, 70);
            
            g.setColor (Color.RED);
            g.drawLine (50, 50, 150, 150);
        }
    }