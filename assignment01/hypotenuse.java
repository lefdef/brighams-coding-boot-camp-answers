import java.util.Scanner;

public class hypotenuse {

	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		double 	a,
				b,
				c;
		//declare doubles since there will be floating points
		
		System.out.print("Enter side A: ");
		a = keyboard.nextDouble();
		
		System.out.print("Enter side B: ");
		b = keyboard.nextDouble();

		c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		//compute using the Pythagorean thereom
		
		System.out.println("The hypotenuse of the triangle is " + c);
				
		
	}
}