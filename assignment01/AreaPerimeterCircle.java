public class AreaPerimeterCircle {

	public static void main(String args[])
	{
		/*Formulas:
		 * Perimeter = 2 * Pi * Radius
		 * Area = Pi * Radius ^ 2
		 */
		double 	area,
				perimeter;
		double	radius = 3.2;
		
		area = Math.PI * Math.pow(radius, 2);
		perimeter = Math.PI * radius * 2;
			
		System.out.println("The perimeter of a circle with a radius of " +
				radius + " inches is " + 
				perimeter + " inches");
		System.out.println("The area of the same circle is " + area);
		
	
	}
}
